package deloitte.academy.lesson5.entity;

import java.util.ArrayList;

import deloitte.academy.lesson5.run.Inventario;

public class Almacen {

	/*
	 * Declaración de las variables que se van a usar a lo largo de todo el proyecto
	 */
	public String nombreDelProducto;
	public double precio;
	private String categoria;
	public String proveedor;
	public double total;
	public int idAlmacen;

	public Almacen(String nombreDelProducto, double precio, String categoria, String proveedor, double total) {
		super();
		this.nombreDelProducto = nombreDelProducto;
		this.precio = precio;
		this.categoria = categoria;
		this.proveedor = proveedor;
		this.total = total;
	}

	public Almacen() {

	}

	public String getNombreDelProducto() {
		return nombreDelProducto;
	}

	public void setNombreDelProducto(String nombreDelProducto) {
		this.nombreDelProducto = nombreDelProducto;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * 
	 * @param newProduct = Producto que se agrega al final de la lista del
	 *                   inventario.
	 */
	public static void agregar(Almacen newProduct) {
		Inventario.listAlmacen.add(newProduct);
	}

	/**
	 * 
	 * @param existentes = productos que se encuentran en el inventario
	 * @param inventario = producto a revisar si se encuentra en el inventario
	 * @return = Lista actualizada despues de remover el articulo
	 */
	public static ArrayList<Almacen> remover(ArrayList<Almacen> existentes, Almacen inventario) {
		ArrayList<Almacen> listaInventario = new ArrayList<Almacen>();
		listaInventario = existentes;

		for (Almacen elemento : existentes) {
			if (elemento.getNombreDelProducto().equals(inventario.getNombreDelProducto())) {
				Inventario.listAlmacen.remove(inventario);
				break;
			}
		}
		return listaInventario;
	}

	/**
	 * Clase que se encarga de actualizar el inventario despues de generar una
	 * compra
	 * 
	 * @param existentes = inventario actual del producto
	 * @param inventario = producto al que s ele restara del stock
	 */
	public static void actual(ArrayList<Almacen> existentes, Almacen inventario) {

		int resta;

		for (Almacen elemento : existentes) {
			if (elemento.getNombreDelProducto().equals(inventario.getNombreDelProducto())) {
				resta = (int) (inventario.getTotal() - 1);

				System.out.println(resta);

				break;
			}
		}
	}
}
