package deloitte.academy.lesson5.enums;

/**
 * Se define los ENUMS y se generan las diferentes categorias
 */
public enum Categorias {
	LapTop("LapTop marca Lenovo gamma media", 1), Electrodomesticos("Cafetera con capacidad M�xima para 8 personas", 2),
	Muebles("Mueble modular para sala", 3), Multi("Sofa tipo reposet", 4);

	public String categoria;
	public int f;

	public int getF() {
		return f;
	}

	public void setF(int f) {
		this.f = f;
	}

	private Categorias(String categoria, int f) {
		this.categoria = categoria;
		this.f = f;

	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
}
