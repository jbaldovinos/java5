package deloitte.academy.lesson5.run;

import deloitte.academy.lesson5.entity.Almacen;
import deloitte.academy.lesson5.enums.Categorias;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Inventario {

	private static final Logger LOGGER = Logger.getLogger(Inventario.class.getName());
	public static final ArrayList<Almacen> listAlmacen = new ArrayList<Almacen>();

	public static void main(String[] args) {

		/**
		 * Declaracion de objetos que se encuentran dentro del Almacen
		 */
		Almacen lista = new Almacen();
		lista.setNombreDelProducto("Laptop");
		lista.setPrecio(2221.50);
		lista.setCategoria(Categorias.LapTop.getCategoria());
		lista.setProveedor("Lenovo");
		lista.setTotal(100);

		Almacen lista1 = new Almacen();
		lista1.setNombreDelProducto("Cafetera");
		lista1.setPrecio(101.50);
		lista1.setCategoria(Categorias.Electrodomesticos.getCategoria());
		lista1.setProveedor("Garat");
		lista1.setTotal(12);

		Almacen lista2 = new Almacen();
		lista2.setNombreDelProducto("Sofa");
		lista2.setPrecio(1250.02);
		lista2.setCategoria(Categorias.Muebles.getCategoria());
		lista2.setProveedor("Multi");
		lista2.setTotal(5);
		System.out.println("Nombre " + "     " + "Precio" + "     " + "Categoria" + "                    " + "Proveedor"
				+ "     " + "Total");
		System.out.println(lista.getNombreDelProducto() + "    " + lista.getPrecio() + "    "
				+ Categorias.LapTop.getCategoria() + "    " + lista.getProveedor() + "    " + lista.getTotal());
		System.out.println(lista1.getNombreDelProducto() + "    " + lista1.getPrecio() + "    "
				+ Categorias.Electrodomesticos.getCategoria() + "    " + lista1.getProveedor() + "    "
				+ lista1.getTotal());
		System.out.println(lista2.getNombreDelProducto() + "    " + lista2.getPrecio() + "    "
				+ Categorias.Muebles.getCategoria() + "    " + lista2.getProveedor() + "    " + lista2.getTotal());

		ArrayList<Almacen> contenidoAlmacen = new ArrayList<Almacen>();

		contenidoAlmacen.add(lista);
		contenidoAlmacen.add(lista1);
		contenidoAlmacen.add(lista2);

		/**
		 * 
		 */

		Almacen nuevoProducto = new Almacen();
		nuevoProducto.setNombreDelProducto("Sillon");
		nuevoProducto.setPrecio(6.40);
		nuevoProducto.setCategoria(Categorias.Multi.getCategoria());
		nuevoProducto.setProveedor("Bic");
		nuevoProducto.setTotal(22);

		/**
		 * Registro de Nuevos Productos
		 */
		try {
			Almacen.agregar(nuevoProducto);
		}

		catch (ArrayIndexOutOfBoundsException ex) {
			LOGGER.log(Level.SEVERE, "Exception occur", ex);

		}
		/**
		 * Remover articulo del Almacen
		 */
		Almacen.remover(contenidoAlmacen, nuevoProducto);

		/**
		 * Actualiza articulos disponibles en inventario
		 */
		Almacen.actual(contenidoAlmacen, nuevoProducto);
		System.out.println(Categorias.Muebles.getCategoria());

	}
}